package com.example.filedownloader.controllers;

import com.example.filedownloader.dtos.Movie;
import com.example.filedownloader.services.FilmService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Map;

@RestController
@RequestMapping("/films")
@RequiredArgsConstructor
public class FilmRestController {
    private final FilmService filmService;

    @PostMapping
    public String uploadFilm(@RequestBody MultipartFile films) throws IOException {
        File convFile = new File( films.getOriginalFilename());
        films.transferTo(convFile);
        this.filmService.loadFilms(convFile);
        return "the file is successfuly uploaded";
    }

    @GetMapping("/{name}")
    public Map<String, Integer> findFilms(@PathVariable String name){
        return this.filmService.findFilmBygenre(name);
    }

    @GetMapping("/search/{year}/{name}")
    public Movie getMovieByNameAndYear(@PathVariable String year, @PathVariable String name){
        return this.filmService.findFilmByYearAndName(year, name);
    }

    @GetMapping("/search/{imdbId}")
    public Movie getMovieByImdbId(@PathVariable String imdbId){
        return this.filmService.findFilmByImdbId(imdbId);
    }
}
