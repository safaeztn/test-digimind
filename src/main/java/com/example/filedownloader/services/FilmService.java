package com.example.filedownloader.services;

import com.example.filedownloader.dtos.Movie;

import java.io.File;
import java.io.IOException;
import java.util.Map;

public interface FilmService {

    void loadFilms(File file) throws IOException;

    Map<String, Integer> findFilmBygenre(String name);

    Movie findFilmByYearAndName(String year, String name);

    Movie findFilmByImdbId(String imdbId);
}
