package com.example.filedownloader.services;

import com.example.filedownloader.dtos.FilmDto;
import com.example.filedownloader.dtos.FilmsList;
import com.example.filedownloader.dtos.Movie;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class FilmServiceImpl implements FilmService {

    @Value("${movie.api-url}")
    private String apiUrl;
    private FilmsList filmsList;

    @Override
    public void loadFilms(File file) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        this.filmsList = objectMapper.readValue(file, FilmsList.class);
    }

    @Override
    public Map<String, Integer> findFilmBygenre(String name) {
        List<FilmDto> films = this.filmsList.getFilms()
                .stream()
                .filter(filmDto -> filmDto.getTitle().toLowerCase().contains(name.toLowerCase()))
                .collect(Collectors.toList());
        Map<String, Integer> filmGenres = new HashMap<>();
        for (FilmDto film : films) {
            if (!film.getGenre().equals("")) {
                filmGenres.put(film.getGenre(), filmGenres.getOrDefault(film.getGenre(), 0) + 1);
            }
        }
        return filmGenres;
    }

    @Override
    public Movie findFilmByYearAndName(String year, String name) {
        WebClient client = WebClient.create();
        WebClient.ResponseSpec responseSpec = client.get()
                .uri(apiUrl + "t=" + name + "&y=" + year)
                .retrieve();
        Movie movieMono = responseSpec.bodyToMono(Movie.class).block();
        return movieMono;
    }

    @Override
    public Movie findFilmByImdbId(String imdbId) {
        WebClient client = WebClient.create();
        WebClient.ResponseSpec responseSpec = client.get()
                .uri(apiUrl + "i=" + imdbId)
                .retrieve();
        Movie movieMono = responseSpec.bodyToMono(Movie.class).block();
        return movieMono;
    }

}
