package com.example.filedownloader.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FilmsList implements Serializable {
    @JsonProperty("films")
    List<FilmDto> films = new ArrayList<>();
}
